# Prepare an array storage of users and assign wines as value for them.
# Prepare an array of wines which is associated with users
# Manage users where wine should be in place with a flag.
# at last, if wines available, manage limit of wines per user and allocate the same
# number of users that need and ready to buy wines

requestFile = 'file.txt'
responseFile = 'output.txt'

import csv
import time

users = dict() 
wines = dict()

# start Request Time


print  "Start Request Time ", time.asctime( time.localtime(time.time()))

# Read file and populate users and wines dictionaries
with open(requestFile, 'rb') as csvfile:
	csv_data = csv.reader(csvfile, delimiter='	')
	for row in csv_data:
		user = row[0]
		wine = row[1]
		if user in users:
			users[user].append(wine)
		else:
			users[user] = [wine]
		if wine in wines:
			wines[wine][0].append(user)
		else:
			wines[wine] = [[], 0]
			wines[wine][0] = [user]


num_wines_sold = 0
solution = dict() 
for user in users:
	avail_wines = [[],[]] #[[wines that are not alloted], [no of users needed wine]]
	for wine in users[user]:
		if wines[wine][1] == 0: # if not taken
			avail_wines[0].append(wine)
			num_potential_buyers = 0
			for potential_buyer in wines[wine][0]:
				if potential_buyer in solution:
					if len(solution[potential_buyer]) <= 3:
						num_potential_buyers += 1
				else:
					num_potential_buyers += 1
			avail_wines[1].append(num_potential_buyers)
	for i in xrange(min([3,len(avail_wines[0])])): # choose up to 3 wines
		if len(avail_wines[0]) > 0:
			min_idx = avail_wines[1].index(min(avail_wines[1]))
			wine = avail_wines[0][min_idx] # wine with least # of user need it
			if user in solution:
				solution[user].append(wine)
			else:
				solution[user] = [wine]
			wines[wine][1] = 1 # marked taken
			num_wines_sold += 1
			avail_wines[0].pop(min_idx)
			avail_wines[1].pop(min_idx)


# export data to file:
with open(responseFile, 'wb') as csvfile:
	csv_out = csv.writer(csvfile, delimiter='	')
	csv_out.writerow([str(num_wines_sold)])
	for user in solution:
		for wine in solution[user]:
			csv_out.writerow([user, wine])

# get duplicate and and max bought report
duplicates = 0
wines_chosen = dict()
max_user = ''
max_buys = 0
for sol_key in solution:
	for w in solution[sol_key]:
		if w in wines_chosen:
			duplicates += 1
		else:
			wines_chosen[w] = 0
	if len(solution[sol_key]) > max_buys:
		max_buys = len(solution[sol_key])
		max_user = sol_key
print 'duplicates: ' + str(duplicates)
print 'max buys: ' + str(max_buys)

print  "End Request Time ", time.asctime( time.localtime(time.time()))

print "Please check output.txt file to see result!";

